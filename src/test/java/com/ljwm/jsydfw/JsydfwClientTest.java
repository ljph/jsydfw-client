package com.ljwm.jsydfw;


import com.xiaoleilu.hutool.http.HttpUtil;
import com.xiaoleilu.hutool.lang.Base64;
import com.xiaoleilu.hutool.util.ReUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

/**
 * Created by yuzhou on 2017/12/25.
 */
public class JsydfwClientTest {

  @Before
  public void setUp() {
    System.out.println("set up");
    String appId = "nusTwV8ATRPoCMqPpVYc";
    String appSercet = "nusTwV8ATRPoCMqPpVYcsQFtPta3G7F8MiKhkyTf5Pp5H9DE3RLmeZ9s3eoRHoMx";
    String serviceUrl = "http://127.0.0.1:5000/TokenWechat/webservice.do";
    String oauthUrl = "https://jxt.jsga.gov.cn:5443/useroauth/getuserid.xhtml?mode=test";

    JsydfwClient.init(appId, appSercet, serviceUrl, oauthUrl);
  }


  @Test
  public void testGetToken() throws Exception {
    System.out.print(JsydfwClient.inst.getToken());
  }

  @Test
  public void testGetUserInfo() throws Exception {
    String code = "2563c854bac1465b94456fb505c95773";
    System.out.print(JsydfwClient.inst.getAuthInfo(code));
  }

  @Test
  public void testHttpJsonGetRequest () {
    String url = "http://localhost:8000/api/auth";
    String userId = "IDCardTest";
    String deviceId = "DeviceIdTest";

    System.out.println(JsydfwClient.inst.httpJsonGetRequest(url, null, userId, deviceId));
  }

  @Test
  public void testBase64Decode() {
    System.out.print(Base64.decodeStr("eyJjb2R1IjowLCJtc2ci0iLmiJDlip8iLCJkYXRhIjpudWxsfQ=="));
  }

  @Test
  public void testE2EAuth() {
    HashMap<String, Object> paramMap = new HashMap<>();
    paramMap.put("code", "2563c854bac1465b94456fb505c95773");
    String result3 = HttpUtil.get("http://localhost:8000/api/auth", paramMap);
    System.out.println(result3);
  }

  @Test
  public void testReplaceStr() {
    String str = "dtt\nddd";
    System.out.println(str);
    System.out.println(str.replaceAll("\n", ""));
  }

  @Test
  public void testExtractCharset() {
    String contentType = "application/json;charset=UTF-8";
    System.out.print(ReUtil.get("charset=(.*)", contentType, 1));
  }

}
