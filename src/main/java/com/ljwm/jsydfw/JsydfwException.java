package com.ljwm.jsydfw;

/**
 * Created by yuzhou on 2017/12/25.
 */
public class JsydfwException extends RuntimeException {

  public JsydfwException(String errorCode, String errorMsg) {
    super(errorMsg);
    this.errorCode = errorCode;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  private String errorCode;

}
